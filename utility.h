// utility.h

#ifndef UTILITY_H
#define UTILITY_H

#include <stdlib.h>
#include <wiringPi.h>

#define ledPinRight1 0 // Right
#define ledPinRight2 3
#define ledPinLeft1 1 // Left
#define ledPinLeft2 4

struct Orientation
{
  float azimuth;
  float pitch;
  float roll;
};

/****************************DECLARATIONS********************************************/

/* parses stream buffer into separate azimuth, pitch, and roll values.
input: char[] formatted with buffer signal at end of each value to be separated.
       struct Orientation.
output: None. (struct Orientation will contain azimuth, pitch, and roll floats.   
complexity: 0(n) , n = length of char[]. */
void convertBuffer(char buffer[], struct Orientation *orientation);


/* initialize GPIO pins.
intput: none
output: 0 if successful, otherwise 1. */
int initializeGPIO();

/* Moves car
input: none
output: none */
void moveForward();
void moveBackward();
void moveLeft();
void moveRight();
void stop();

/* Moves car based on  input.
input: Pitch, Roll.
ouput: none. */
void moveCar(struct Orientation* orientation);

/***************************DEFINITIONS***********************************/

void moveForward()
{

    digitalWrite(ledPinLeft1, LOW);

    //digitalWrite(ledPinLeft2, HIGH);
    __asm volatile ("mov r1, #1");
    __asm volatile ("mov r0, #4");
    __asm volatile ("bl digitalWrite");

    digitalWrite(ledPinRight1, LOW);

    //digitalWrite(ledPinRight2, HIGH);
    __asm volatile ("mov r1, #1");
    __asm volatile ("mov r0, #3");
    __asm volatile ("bl digitalWrite");
}

void moveBackward()
{
    //digitalWrite(ledPinLeft1, HIGH);
    __asm volatile ("mov r1, #1");
    __asm volatile ("mov r0, #1");
    __asm volatile ("bl digitalWrite");

    digitalWrite(ledPinLeft2, LOW);

    //digitalWrite(ledPinRight1, HIGH);
    __asm volatile ("mov r1, #1");
    __asm volatile ("mov r0, #0");
    __asm volatile ("bl digitalWrite");
   
    digitalWrite(ledPinRight2, LOW);
}

void moveLeft()
{
  //digitalWrite(ledPinRight1, LOW);
  __asm volatile ("mov r1, #0");
  __asm volatile ("mov r0, #0");
  __asm volatile ("bl digitalWrite");

  //digitalWrite(ledPinRight2, HIGH);
  __asm volatile ("mov r1, #1");
  __asm volatile ("mov r0, #3");
  __asm volatile ("bl digitalWrite");
  
  digitalWrite(ledPinLeft1, LOW);
  digitalWrite(ledPinLeft2, LOW);
  
}

void moveRight()
{  
    //digitalWrite(ledPinLeft1, LOW);
    __asm volatile ("mov r1, #0");
    __asm volatile ("mov r1, #1");
    __asm volatile ("bl digitalWrite");

    //digitalWrite(ledPinLeft2, HIGH);
    __asm volatile ("mov r1, #1");
    __asm volatile ("mov r0, #4");
    __asm volatile ("bl digitalWrite");
    
    digitalWrite(ledPinRight1, LOW);
    digitalWrite(ledPinRight2, LOW);
}

void stop()
{
    digitalWrite(ledPinRight1, LOW);
    digitalWrite(ledPinRight2, LOW);
    digitalWrite(ledPinLeft1, LOW);
    digitalWrite(ledPinLeft2, LOW);
}

void moveCar(struct Orientation* orientation )
{
  //float res = 0;
  
  /*
  asm (
       //mov %r[result], %r[input_pitch]
       //"vadd %r[result], %r[input_pitch], %r[input_roll]"
       
       "mov r1, %r[input_pitch]\n\t"
       "subs r1, #0.3\n\t"
       //"cmp r1, #0.3\n\t"
       //"cmp %r[input_pitch], #0.3\n\t"
       "bgt forward\n\t"
       "mov %r[result], %r[input_pitch]\n\t"

       "forward:\n\t"
       "mov %r[result], %r[input_roll]\n\t"

       : [result] "=r" (res)
       : [input_pitch] "r" (orientation->pitch), [input_roll] "r" (orientation->roll)
       );

  printf("res = %f\n", res);
  */ 

  
  printf("\n\nmoveCar.. Azimuth = %f .. Pitch = %f .. Roll = %f \n\n", orientation->azimuth, orientation->pitch, orientation->roll);
  
  if (orientation->pitch > 0.3)
    {
      moveForward();
    }
  else if (orientation->pitch < -0.3)
    {
      moveBackward();
    }
  else if (orientation->roll > 0.6)
    {
      moveRight();
    }
  else if (orientation->roll < -0.6)
    {
      moveLeft();
    }
  else
    {
      stop();
    }
  
  
}


int initializeGPIO()
{
  
    if (wiringPiSetup() == -1)
    {
        printf("setup wiringPi failed!");
        return 1;
    }

    pinMode(ledPinRight1, OUTPUT);
    pinMode(ledPinRight2, OUTPUT);
    pinMode(ledPinLeft1, OUTPUT);
    pinMode(ledPinLeft2, OUTPUT);

    return 0;
}


void convertBuffer(char buffer[], struct Orientation *orientation)
{
  int bufferSignalCount = 0;

  int bufferIndex = 0;
  int azimuthIndex = 0;
  int pitchIndex = 0;
  int rollIndex = 0;
  
  // parsed subsets
  char azimuth[20] = "";
  char pitch[20] = "";
  char roll[20]= "";

  char bufferSignal = 'x';  

  while (bufferSignalCount < 3)
  {
    
    switch (bufferSignalCount)
    {
      
      case 0:
	
        while (buffer[bufferIndex] != bufferSignal)
	{
	  azimuth[azimuthIndex] = buffer[bufferIndex];
	  azimuthIndex++;
	  bufferIndex++;
	}

	printf("azimuth = %s \n", azimuth);
	
	bufferIndex++;
	bufferSignalCount++;

	orientation->azimuth = strtof(azimuth, NULL);
	
	break;
	
      case 1:
	
        while (buffer[bufferIndex] != bufferSignal)
	{
	  pitch[pitchIndex] = buffer[bufferIndex];	  
	  pitchIndex++;
	  bufferIndex++; 
	}
	
	//printf("pitch = %s \n", pitch);
	
        bufferSignalCount++;
	bufferIndex++;
	
	orientation->pitch = strtof(pitch, NULL);

	printf("pitch = %f \n", orientation->pitch);

	
	break;
	
      case 2:
	
	while (buffer[bufferIndex] != bufferSignal)
	{
	  roll[rollIndex] = buffer[bufferIndex];
	  rollIndex++;
	  bufferIndex++;
	}

	printf("roll = %s \n", roll);
	
	bufferIndex++;
	bufferSignalCount++;
	
	orientation->roll = strtof(roll, NULL);
	
	break;
	
      default:
	
	bufferSignalCount++; 
	break;
    }       
  }   
}



#endif
