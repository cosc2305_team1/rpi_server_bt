#include <wiringPi.h>
#include <stdio.h>

#define ledPin 24

int assemblyFunction(int i, int j)
{
  int res = 0;
  asm (
       "add %r[result], %r[input_i], %r[input_j]"
       : [result] "=r" (res)
       : [input_i] "r" (i), [input_j] "r" (j)
       );
  return res;
}

int main(void)
{
    if(wiringPiSetup() == -1) {
        printf("setup wiringPi failed!");
        return 1;
    }
  
    printf("wiringPi initialize successfully, GPIO %d(wiringPi pin)\n", ledPin);

    pinMode(ledPin, OUTPUT);

    while(1) {

      int result = 0;
      int i = 1;
      int j = 3;
      result = assemblyFunction(i, j);
      printf("%d\n", result);
      
      //digitalWrite(ledPin, HIGH);
      //printf("led on...\n");
      
      delay(1000);
      
      digitalWrite(ledPin, LOW);
      printf("...led off\n");
      delay(1000);
    }

    return 0;
}
