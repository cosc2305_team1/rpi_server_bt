/*  
 * file: server.c
 * author: Team 1
 *
 * source copied and modified from:
 * https://people.csail.mit.edu/albert/bluez-intro/index.html
 * Copyright � 2005-2008 Albert Huang.
 * Permission is granted to copy, distribute and/or modify this document
 * under the terms of the GNU Free Documentation License, Version 1.2
 * or any later version published by the Free Software Foundation;
 * with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 * A copy of the license is included in the section entitled "GNU
 * Free Documentation License".
 *
 * compilation:$ gcc -o server server.c -lbluetooth -lwiringPi
 *
 * program starts on RPi boot:
 * /etc/rc.local
 * sudo ./home/pi/server &
*/

#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

#include "utility.h"

int main(int argc, char **argv) {

  
    printf("Server begin....\n");
   
    struct sockaddr_rc loc_addr = { 0 }; // local rfcomm socket address
    struct sockaddr_rc rem_addr = { 0 }; // socket descriptor for remote client
    char buffer[1024] = { 0 };
    int localSocket; // socket descriptor for local listener
    int remoteSocket; // socket descriptor for remote client
    int bytes_read;
    socklen_t opt = sizeof(rem_addr);
    int init_enabled = 1;
    int client_disconnect_signal;

    struct Orientation orientation = {0,0,0};

    printf("Init complete.\n");

    while (init_enabled) {

        int convertBufferCount = 0;

        printf("Init complete.\n");
	
        client_disconnect_signal = 0;
	  
        // initialize a bluetooth socket
        localSocket = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

        printf("socket allocated.\n");

        // bind socket to port 1 of the first available local bluetooth adapter.
        loc_addr.rc_family = AF_BLUETOOTH;
        loc_addr.rc_bdaddr = *BDADDR_ANY;
        loc_addr.rc_channel = (uint8_t) 1;
        bind(localSocket, (struct sockaddr *)&loc_addr, sizeof(loc_addr));
    
        // put socket into listening mode.
        listen(localSocket, 1);
        printf("Server listening...\n");

        // accept one connection.
        remoteSocket = accept(localSocket, (struct sockaddr *)&rem_addr, &opt);


        printf("client connection established.\n");

	ba2str( &rem_addr.rc_bdaddr, buffer );
	fprintf(stderr, "accepted connection from %s\n", buffer);
	memset(buffer, 0, sizeof(buffer));

	initializeGPIO();
	
	while (!client_disconnect_signal) {
	  
	  bytes_read = read(remoteSocket, buffer, sizeof(buffer)); // buffer reads new values.
	    
	    if (bytes_read > 0) {
	      
	        printf("\nIncoming buffer: %s \n", buffer);

	        if (buffer[0] == 'd') {
		    client_disconnect_signal = 1;
		}
		else {
		    convertBufferCount++;
		    printf("convertBuffer() called: %d\n", convertBufferCount);
		    convertBuffer(buffer, &orientation);
		    moveCar(&orientation);
		}	      
	    } 
	    else {
	      // if bytes_read = 0, client disconnected.
	      client_disconnect_signal = 1;
	    }

	    	       
	}

	stop();
	printf("Closing Connection....\n");
	close(remoteSocket);
	close(localSocket);
    	  
    } // end while
    
    return 0;
}

